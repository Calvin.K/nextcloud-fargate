CREATE DATABASE IF NOT EXISTS nextcloud;
--Passwort von root user ändern
ALTER USER 'root'@'localhost' IDENTIFIED BY 'bbwadmin';
--admin user erstellen
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin1234';
--admin alle rechte geben
GRANT ALL PRIVILEGES ON nextcloud.* TO 'admin'@'localhost';
--änderungen aktivieren
FLUSH PRIVILEGES;